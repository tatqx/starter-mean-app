app.controller('LoginController', function($scope, $http, Identity, Notifier, Auth) {

    $scope.identity = Identity;
	$scope.signin = function (username, password) {

        Auth.authenticateUser(username, password).then(function (success) {

            if (success) {
                Notifier.notify('Logged in');
            } else {
                Notifier.notify('Failed to log in!');
            }
        });
	};
});