angular.module('app').factory('Identity', function () {
    return {
      currentUser: undefined,
      isAuthenticated: function () {
          return !!this.currentUser;
      }
    };
});