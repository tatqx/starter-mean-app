
var mongoose = require('mongoose');

module.exports = function (config) {

	mongoose.connect(config.db);
	var db = mongoose.connection;

	db.on('error', console.error.bind(console, 'Database connection error...'));
	db.once('open', function callback() {
		console.log('meanapp database opened');
	});

	var userSchema = mongoose.Schema({
		userName: String
	});

	var User = mongoose.model('User', userSchema);

	User.find({}).exec(function (err, collection) {

		if (collection.length == 0) {
			User.create({userName:'tat'});
			User.create({userName:'bat'});
			User.create({userName:'cat'});
		}
	});
};
