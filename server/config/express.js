
var express = require('express'),
	logger = require('morgan'),
	bodyParser = require('body-parser'),
	passport = require('passport'),
	cookieParser = require('cookie-parser'),
	session = require('express-session');

module.exports = function (app, config) {

	app.set('views', config.rootPath + '/server/views' );
	app.set('view engine', 'ejs');

	app.use(express.static(config.rootPath + "/public"));
	app.use(logger('dev'));
	app.use(cookieParser());
	app.use(session({secret:'mean app secret'}));
	app.use(bodyParser());
	app.use(passport.initialize());
	app.use(passport.session());
};
